package ru.bespalov.weatherapp.domain.model

data class HourlyPart(
    val dt: String,
    val temp: Double,
    val feels_like: Double,
    val weather: List<WeatherMainInfo>
)
