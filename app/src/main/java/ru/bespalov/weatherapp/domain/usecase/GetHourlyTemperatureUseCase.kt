package ru.bespalov.weatherapp.domain.usecase

import ru.bespalov.weatherapp.domain.WeatherRepository
import ru.bespalov.weatherapp.domain.model.Coordinate
import ru.bespalov.weatherapp.domain.model.HourlyPart
import ru.bespalov.weatherapp.presentation.enums.WeatherPeriod
import javax.inject.Inject

class GetHourlyTemperatureUseCase @Inject constructor(
    private val weatherRepository: WeatherRepository
) {
    companion object {
        private const val HOURS_IN_DAY = 24
    }
    suspend fun execute(coordinate: Coordinate, period: WeatherPeriod = WeatherPeriod.TODAY): List<HourlyPart> {
        if (period == WeatherPeriod.TODAY) {
            return weatherRepository.getWeather(coordinate).hourly.take(HOURS_IN_DAY)
        } else if (period == WeatherPeriod.TOMORROW) {
            return weatherRepository.getWeather(coordinate).hourly.takeLast(HOURS_IN_DAY)
        }
        return emptyList()
    }
}