package ru.bespalov.weatherapp.domain.model

data class DailyTemperature(
    val min: String,
    val day: String,
    val max: String,
    val night: String,
    val eve: String,
    val morn: String
)
