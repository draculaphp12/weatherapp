package ru.bespalov.weatherapp.domain.model

data class Weather(
    val hourly: List<HourlyPart>,
    val daily: List<DailyPart>
)
