package ru.bespalov.weatherapp.domain.model

data class City(
    val name: String,
    val coordinate: Coordinate
) {
    override fun toString(): String {
        return name
    }
}
