package ru.bespalov.weatherapp.domain

import ru.bespalov.weatherapp.domain.model.City

interface CityRepository {
    suspend fun getAll(): List<City?>?

    suspend fun addCity(city: City)
}