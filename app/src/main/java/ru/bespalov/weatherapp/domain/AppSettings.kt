package ru.bespalov.weatherapp.domain

import ru.bespalov.weatherapp.domain.model.Coordinate

interface AppSettings {
    fun getCurrentCity(): String?

    fun setCurrentCity(cityName: String)

    fun setCurrentCityCoordinate(lat: Float, lon: Float)

    fun getCurrentCityCoordinate(): Coordinate

    fun isCurrentCityExists(): Boolean
}