package ru.bespalov.weatherapp.domain

import ru.bespalov.weatherapp.domain.model.Coordinate
import ru.bespalov.weatherapp.domain.model.Weather

interface WeatherRepository {
    suspend fun getWeather(coordinate: Coordinate): Weather

    suspend fun getCityCoordinatesByName(cityName: String): Coordinate?
}