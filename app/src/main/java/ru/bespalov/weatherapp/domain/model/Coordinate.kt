package ru.bespalov.weatherapp.domain.model

data class Coordinate(
    val lat: Float,
    val lon: Float
)
