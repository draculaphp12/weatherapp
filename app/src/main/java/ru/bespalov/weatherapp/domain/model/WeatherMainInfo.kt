package ru.bespalov.weatherapp.domain.model

data class WeatherMainInfo(
    val id: Int,
    val main: String,
    val description: String,
    val icon: String
)