package ru.bespalov.weatherapp.domain.model

data class DailyPart(
    val dt: String,
    val temp: DailyTemperature,
    val weather: List<WeatherMainInfo>
)