package ru.bespalov.weatherapp.domain.usecase

import ru.bespalov.weatherapp.domain.WeatherRepository
import ru.bespalov.weatherapp.domain.model.Coordinate
import javax.inject.Inject

class GetCityCoordinateUseCase @Inject constructor(
    private val weatherRepository: WeatherRepository
) {
    suspend fun execute(cityName: String): Coordinate? {
        return weatherRepository.getCityCoordinatesByName(cityName)
    }
}