package ru.bespalov.weatherapp.domain.usecase

import ru.bespalov.weatherapp.domain.WeatherRepository
import ru.bespalov.weatherapp.domain.model.Coordinate
import ru.bespalov.weatherapp.domain.model.DailyPart
import javax.inject.Inject

class GetDailyWeatherUseCase @Inject constructor(
    private val weatherRepository: WeatherRepository
) {
    companion object {
        private const val DAY_IN_WEEK = 7
    }
    suspend fun execute(coordinate: Coordinate): List<DailyPart> {
        return weatherRepository.getWeather(coordinate).daily.take(DAY_IN_WEEK)
    }
}