package ru.bespalov.weatherapp.domain.usecase

import ru.bespalov.weatherapp.domain.CityRepository
import ru.bespalov.weatherapp.domain.model.City
import javax.inject.Inject

class AddCityUseCase @Inject constructor(
    private val cityRepository: CityRepository
) {
    suspend fun execute(city: City) {
        cityRepository.addCity(city)
    }
}