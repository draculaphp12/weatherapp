package ru.bespalov.weatherapp.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.HttpException
import ru.bespalov.weatherapp.domain.AppSettings
import ru.bespalov.weatherapp.domain.model.HourlyPart
import ru.bespalov.weatherapp.domain.usecase.GetHourlyTemperatureUseCase
import ru.bespalov.weatherapp.presentation.enums.WeatherPeriod
import javax.inject.Inject

@HiltViewModel
class HourlyViewModel @Inject constructor(
    private val appSettings: AppSettings,
    private val getHourlyTemperatureUseCase: GetHourlyTemperatureUseCase
) : ViewModel() {

    private val _hourlyWeather = MutableLiveData<List<HourlyPart>>()
    val hourlyWeather: LiveData<List<HourlyPart>>
        get() = _hourlyWeather

    fun loadHourlyWeather(period: WeatherPeriod) {
        viewModelScope.launch {
            val coordinate = appSettings.getCurrentCityCoordinate()
            try {
                _hourlyWeather.value = getHourlyTemperatureUseCase.execute(coordinate, period)
            } catch (e: Exception) {
                _hourlyWeather.value = listOf()
            }
        }
    }
}