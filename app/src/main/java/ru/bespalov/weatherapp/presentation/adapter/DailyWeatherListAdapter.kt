package ru.bespalov.weatherapp.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ru.bespalov.weatherapp.R
import ru.bespalov.weatherapp.databinding.DailyWeatherItemBinding
import ru.bespalov.weatherapp.domain.model.DailyPart
import java.text.SimpleDateFormat
import java.util.*

class DailyWeatherListAdapter(
    private val dailyWeatherList: List<DailyPart>
) : RecyclerView.Adapter<DailyWeatherListAdapter.DailyWeatherVH>() {
    companion object {
        private const val IMAGE_URL_BASE = "http://openweathermap.org/img/wn/"
        private const val IMAGE_SUFFIX = "@2x.png"
    }

    inner class DailyWeatherVH(
        private val itemBinding: DailyWeatherItemBinding
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        private val simpleDateFormat = SimpleDateFormat("dd MMMM", Locale.ENGLISH)

        fun bind(item: DailyPart) {
            val weather = item.weather.first()
            val imageUrl = IMAGE_URL_BASE + weather.icon + IMAGE_SUFFIX
            Glide.with(itemView.context)
                .load(imageUrl)
                .into(itemBinding.dailyWeatherIcon)
            itemBinding.day.text = simpleDateFormat.format(item.dt.toLong() * 1000L)
            itemBinding.dailyWeatherDescription.text = weather.description
            itemBinding.dayTemperature.text = String.format(itemView.resources.getString(R.string.day_temperature), item.temp.day)
            itemBinding.nightTemperature.text = String.format(itemView.resources.getString(R.string.night_temperature), item.temp.night)
            itemBinding.minTemperature.text = String.format(itemView.resources.getString(R.string.min_day_temperature), item.temp.min)
            itemBinding.maxTemperature.text = String.format(itemView.resources.getString(R.string.max_day_temperature), item.temp.max)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DailyWeatherVH {
        val rootView = DailyWeatherItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DailyWeatherVH(rootView)
    }

    override fun onBindViewHolder(holder: DailyWeatherVH, position: Int) {
        holder.bind(dailyWeatherList[position])
    }

    override fun getItemCount(): Int {
        return dailyWeatherList.size
    }
}