package ru.bespalov.weatherapp.presentation.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.lifecycle.Lifecycle
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import ru.bespalov.weatherapp.R
import ru.bespalov.weatherapp.databinding.FragmentMenuBinding
import ru.bespalov.weatherapp.domain.AppSettings
import ru.bespalov.weatherapp.presentation.adapter.PagerAdapter
import javax.inject.Inject

@AndroidEntryPoint
class MenuFragment : Fragment() {

    private lateinit var _binding: FragmentMenuBinding
    @Inject
    lateinit var appSettings: AppSettings

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMenuBinding.inflate(inflater, container, false)
        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().title = appSettings.getCurrentCity()

        val pagerAdapter = PagerAdapter(this)
        _binding.viewPager2.adapter = pagerAdapter

        TabLayoutMediator(_binding.tabMenu, _binding.viewPager2) {
            tab, pos -> when (pos) {
                0 -> tab.text = requireActivity().resources.getString(R.string.today_tab_item)
                1 -> tab.text = requireActivity().resources.getString(R.string.tomorrow_tab_item)
                else -> tab.text = requireActivity().resources.getString(R.string.week_tab_item)
            }
        }.attach()

        val menuHost: MenuHost = requireActivity()

        menuHost.addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.main_menu, menu)
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when (menuItem.itemId) {
                    R.id.action_add_city -> {
                        requireActivity().supportFragmentManager.beginTransaction()
                            .replace(R.id.mainContainer, StartFragment())
                            .commit()
                        true
                    }
                    R.id.action_change_city -> {
                        requireActivity().supportFragmentManager.beginTransaction()
                            .replace(R.id.mainContainer, ChangeCityFragment())
                            .commit()
                        true
                    }
                    else -> false
                }
            }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)

    }
}