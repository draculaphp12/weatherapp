package ru.bespalov.weatherapp.presentation.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.bespalov.weatherapp.R
import ru.bespalov.weatherapp.databinding.FragmentChangeCityBinding
import ru.bespalov.weatherapp.domain.model.City
import ru.bespalov.weatherapp.presentation.enums.Status
import ru.bespalov.weatherapp.presentation.viewmodel.ChangeCityViewModel

@AndroidEntryPoint
class ChangeCityFragment : Fragment() {
    private lateinit var _binding: FragmentChangeCityBinding
    private val viewModel by viewModels<ChangeCityViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentChangeCityBinding.inflate(inflater, container, false)
        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.cities.observe(viewLifecycleOwner) {
            val adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, it)
            _binding.citySpinner.adapter = adapter
        }

        _binding.changeCityButton.setOnClickListener {
            val selectedCity = _binding.citySpinner.selectedItem as City
            viewModel.changeCity(selectedCity)
        }

        viewModel.status.observe(viewLifecycleOwner) {
            if (it == Status.SUCCESS) {
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.mainContainer, MenuFragment())
                    .commit()
            }
        }
    }
}