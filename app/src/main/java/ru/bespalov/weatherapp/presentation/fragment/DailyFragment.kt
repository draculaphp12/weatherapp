package ru.bespalov.weatherapp.presentation.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ru.bespalov.weatherapp.R
import ru.bespalov.weatherapp.databinding.FragmentDailyBinding
import ru.bespalov.weatherapp.domain.model.DailyPart
import ru.bespalov.weatherapp.presentation.adapter.DailyWeatherListAdapter
import ru.bespalov.weatherapp.presentation.viewmodel.DailyViewModel

@AndroidEntryPoint
class DailyFragment : Fragment() {

    private lateinit var _binding: FragmentDailyBinding

    private val viewModel by viewModels<DailyViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDailyBinding.inflate(inflater, container, false)
        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.dailyWeather.observe(viewLifecycleOwner) {
            setupList(it)
        }
    }

    private fun setupList(dailyList: List<DailyPart>) {
        _binding.dailyWeatherList.layoutManager = LinearLayoutManager(requireContext())
        _binding.dailyWeatherList.adapter = DailyWeatherListAdapter(dailyList)
    }
}