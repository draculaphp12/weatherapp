package ru.bespalov.weatherapp.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.HttpException
import ru.bespalov.weatherapp.domain.AppSettings
import ru.bespalov.weatherapp.domain.model.DailyPart
import ru.bespalov.weatherapp.domain.usecase.GetDailyWeatherUseCase
import javax.inject.Inject

@HiltViewModel
class DailyViewModel @Inject constructor(
    private val appSettings: AppSettings,
    private val getDailyWeatherUseCase: GetDailyWeatherUseCase
) : ViewModel() {

    private val _dailyWeather = MutableLiveData<List<DailyPart>>()
    val dailyWeather: LiveData<List<DailyPart>>
        get() = _dailyWeather

    init {
        loadDailyWeather()
    }

    private fun loadDailyWeather() {
        viewModelScope.launch {
            try {
                _dailyWeather.value = getDailyWeatherUseCase.execute(appSettings.getCurrentCityCoordinate())
            } catch (e: Exception) {
                _dailyWeather.value = emptyList()
            }
        }
    }
}