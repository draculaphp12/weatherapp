package ru.bespalov.weatherapp.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ru.bespalov.weatherapp.R
import ru.bespalov.weatherapp.databinding.HourlyWeatherItemBinding
import ru.bespalov.weatherapp.domain.model.HourlyPart
import java.text.SimpleDateFormat
import java.util.*

class HourlyWeatherListAdapter(
    private val hourlyWeatherList: List<HourlyPart>
) : RecyclerView.Adapter<HourlyWeatherListAdapter.WeatherVH>() {

    companion object {
        private const val IMAGE_URL_BASE = "http://openweathermap.org/img/wn/"
        private const val IMAGE_SUFFIX = "@2x.png"
    }

    inner class WeatherVH(
        private val itemBinding: HourlyWeatherItemBinding
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        private val simpleDateFormat = SimpleDateFormat("dd MMMM HH:mm", Locale.ENGLISH)

        fun bind(item: HourlyPart) {
            val weather = item.weather.first()
            val imageUrl = IMAGE_URL_BASE + weather.icon + IMAGE_SUFFIX
            Glide.with(itemView.context)
                .load(imageUrl)
                .into(itemBinding.weatherIcon)
            itemBinding.datetime.text = simpleDateFormat.format(item.dt.toLong() * 1000L)
            itemBinding.weatherDescription.text = weather.description
            itemBinding.realTemperature.text = item.temp.toString()
            itemBinding.feelsLike.text = String.format(itemView.resources.getString(R.string.feels_like), item.feels_like.toString())
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherVH {
        val rootView = HourlyWeatherItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return WeatherVH(rootView)
    }

    override fun onBindViewHolder(holder: WeatherVH, position: Int) {
        holder.bind(hourlyWeatherList[position])
    }

    override fun getItemCount(): Int {
        return hourlyWeatherList.size
    }
}