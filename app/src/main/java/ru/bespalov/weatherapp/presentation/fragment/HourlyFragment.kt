package ru.bespalov.weatherapp.presentation.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ru.bespalov.weatherapp.databinding.FragmentHourlyBinding
import ru.bespalov.weatherapp.domain.model.HourlyPart
import ru.bespalov.weatherapp.presentation.viewmodel.HourlyViewModel
import ru.bespalov.weatherapp.presentation.adapter.HourlyWeatherListAdapter
import ru.bespalov.weatherapp.presentation.enums.WeatherPeriod

@AndroidEntryPoint
class HourlyFragment : Fragment() {

    private lateinit var _binding: FragmentHourlyBinding

    private val viewModel by viewModels<HourlyViewModel>()

    private var period: String? = WeatherPeriod.TODAY.toString()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            period = it.getString(PERIOD_ARG)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHourlyBinding.inflate(inflater, container, false)
        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.loadHourlyWeather(WeatherPeriod.valueOf(period!!))
        viewModel.hourlyWeather.observe(viewLifecycleOwner) {
            setupWeatherList(it)
        }
    }

    private fun setupWeatherList(items: List<HourlyPart>) {
        _binding.hourlyWeatherList.layoutManager = LinearLayoutManager(requireContext())
        _binding.hourlyWeatherList.adapter = HourlyWeatherListAdapter(items)
    }

    companion object {
        private const val PERIOD_ARG = "period_arg"
        @JvmStatic
        fun newInstance(period: WeatherPeriod) = HourlyFragment().apply {
            arguments = Bundle().apply {
                putString(PERIOD_ARG, period.toString())
            }
        }
    }

}