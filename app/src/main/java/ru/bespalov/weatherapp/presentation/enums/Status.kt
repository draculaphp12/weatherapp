package ru.bespalov.weatherapp.presentation.enums

enum class Status {
    SUCCESS, ERROR, NOT_STARTED
}