package ru.bespalov.weatherapp.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import ru.bespalov.weatherapp.domain.AppSettings
import ru.bespalov.weatherapp.domain.model.City
import ru.bespalov.weatherapp.domain.usecase.GetAllCityUseCase
import ru.bespalov.weatherapp.presentation.enums.Status
import javax.inject.Inject

@HiltViewModel
class ChangeCityViewModel @Inject constructor(
    private val getAllCityUseCase: GetAllCityUseCase,
    private val appSettings: AppSettings
) : ViewModel() {

    private val _cities = MutableLiveData<List<City>>()
    val cities: LiveData<List<City>>
        get() = _cities

    private val _status = MutableLiveData(Status.NOT_STARTED)
    val status: LiveData<Status>
        get() = _status

    init {
        loadCities()
    }

    private fun loadCities() {
        viewModelScope.launch {
            try {
                _cities.value = getAllCityUseCase.execute()
            } catch (e: Exception) {
                _cities.value = emptyList()
            }
        }
    }

    fun changeCity(city: City) {
        try {
            appSettings.setCurrentCity(city.name)
            appSettings.setCurrentCityCoordinate(city.coordinate.lat, city.coordinate.lon)
            _status.value = Status.SUCCESS
        } catch (e: Exception) {
            _status.value = Status.ERROR
        }
    }
}