package ru.bespalov.weatherapp.presentation.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.bespalov.weatherapp.R
import ru.bespalov.weatherapp.databinding.FragmentStartBinding
import ru.bespalov.weatherapp.domain.AppSettings
import ru.bespalov.weatherapp.presentation.viewmodel.StartViewModel
import ru.bespalov.weatherapp.presentation.enums.Status
import javax.inject.Inject

@AndroidEntryPoint
class StartFragment : Fragment() {

    private lateinit var _binding: FragmentStartBinding
    private val viewModel by viewModels<StartViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentStartBinding.inflate(inflater, container, false)
        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.status.observe(viewLifecycleOwner) {
            if (it == Status.ERROR) {
                _binding.errorTextView.visibility = View.VISIBLE
            } else if (it == Status.SUCCESS) {
                _binding.errorTextView.visibility = View.GONE
                requireActivity().supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.mainContainer, MenuFragment())
                    .commit()
            }
        }

        _binding.setCityButton.setOnClickListener {
            viewModel.setCity(_binding.setCityEditText.text.toString())
        }
    }
}