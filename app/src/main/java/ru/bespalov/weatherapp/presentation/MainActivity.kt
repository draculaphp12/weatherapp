package ru.bespalov.weatherapp.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import dagger.hilt.android.AndroidEntryPoint
import ru.bespalov.weatherapp.R
import ru.bespalov.weatherapp.databinding.ActivityMainBinding
import ru.bespalov.weatherapp.domain.AppSettings
import ru.bespalov.weatherapp.presentation.fragment.MenuFragment
import ru.bespalov.weatherapp.presentation.fragment.StartFragment
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject lateinit var appSettings: AppSettings

    private val _binding: ActivityMainBinding
        get() = ActivityMainBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(_binding.root)

        if (!appSettings.isCurrentCityExists()) {
            supportFragmentManager.beginTransaction()
                .add(R.id.mainContainer, StartFragment())
                .commit()
        } else {
            title = appSettings.getCurrentCity()
            supportFragmentManager.beginTransaction()
                .add(R.id.mainContainer, MenuFragment())
                .commit()
        }
    }
}