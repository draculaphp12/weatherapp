package ru.bespalov.weatherapp.presentation.enums

enum class WeatherPeriod {
    TODAY, TOMORROW, WEEK
}