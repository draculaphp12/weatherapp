package ru.bespalov.weatherapp.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import ru.bespalov.weatherapp.domain.AppSettings
import ru.bespalov.weatherapp.domain.model.City
import ru.bespalov.weatherapp.domain.usecase.AddCityUseCase
import ru.bespalov.weatherapp.domain.usecase.GetCityCoordinateUseCase
import ru.bespalov.weatherapp.presentation.enums.Status
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class StartViewModel @Inject constructor(
    private val appSettings: AppSettings,
    private val getCityCoordinateUseCase: GetCityCoordinateUseCase,
    private val addCityUseCase: AddCityUseCase
) : ViewModel() {

    private val _status = MutableLiveData(Status.NOT_STARTED)
    val status: LiveData<Status>
        get() = _status

    fun setCity(cityName: String) {
        viewModelScope.launch {
            try {
                val coordinates = getCityCoordinateUseCase.execute(cityName)
                if (coordinates == null) {
                    _status.value = Status.ERROR
                } else {
                    _status.value = Status.SUCCESS
                    appSettings.setCurrentCity(cityName)
                    appSettings.setCurrentCityCoordinate(coordinates.lat, coordinates.lon)
                    addCityUseCase.execute(City(cityName, coordinates))
                }
            } catch (e: Exception) {
                _status.value = Status.ERROR
            }
        }
    }
}