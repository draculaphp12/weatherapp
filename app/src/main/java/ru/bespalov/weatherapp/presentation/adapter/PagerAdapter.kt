package ru.bespalov.weatherapp.presentation.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import ru.bespalov.weatherapp.presentation.enums.WeatherPeriod
import ru.bespalov.weatherapp.presentation.fragment.DailyFragment
import ru.bespalov.weatherapp.presentation.fragment.MenuFragment
import ru.bespalov.weatherapp.presentation.fragment.HourlyFragment

class PagerAdapter(fragment: MenuFragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int {
        return 3
    }

    override fun createFragment(position: Int): Fragment {
        return when(position) {
            0 -> {
                HourlyFragment.newInstance(WeatherPeriod.TODAY)
            }
            1 -> {
                HourlyFragment.newInstance(WeatherPeriod.TOMORROW)
            }
            else -> {
                DailyFragment()
            }
        }
    }
}