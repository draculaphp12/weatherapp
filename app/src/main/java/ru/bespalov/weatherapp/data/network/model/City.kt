package ru.bespalov.weatherapp.data.network.model

import com.google.gson.annotations.SerializedName
import ru.bespalov.weatherapp.domain.model.Coordinate

data class City(
    @SerializedName("coord") val coordinate: Coordinate,
    val name: String
) {
    fun toCoordinate(): Coordinate = Coordinate(
        lat = coordinate.lat,
        lon = coordinate.lon
    )
}