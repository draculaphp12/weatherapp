package ru.bespalov.weatherapp.data.network.repository

import retrofit2.HttpException
import ru.bespalov.weatherapp.data.network.service.WeatherService
import ru.bespalov.weatherapp.domain.WeatherRepository
import ru.bespalov.weatherapp.domain.model.Coordinate
import ru.bespalov.weatherapp.domain.model.Weather
import javax.inject.Inject

class WeatherRepositoryImpl @Inject constructor(
    private val weatherService: WeatherService
) : WeatherRepository {
    override suspend fun getWeather(coordinate: Coordinate): Weather {
        return weatherService.getWeatherAsync(coordinate.lat, coordinate.lon).await()
    }

    override suspend fun getCityCoordinatesByName(cityName: String): Coordinate? {
        return try {
            weatherService.getCoordinatesByCityNameAsync(cityName).await().toCoordinate()
        } catch (e: HttpException) {
            null
        }
    }
}