package ru.bespalov.weatherapp.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import ru.bespalov.weatherapp.data.db.dao.CityDao
import ru.bespalov.weatherapp.data.db.entity.CityEntity

@Database(
    version = 1,
    entities = [
        CityEntity::class
    ]
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getCityDao(): CityDao
}