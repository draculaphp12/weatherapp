package ru.bespalov.weatherapp.data.network.service

import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query
import ru.bespalov.weatherapp.data.network.model.City
import ru.bespalov.weatherapp.domain.model.Weather

interface WeatherService {
    companion object {
        private const val API_KEY = "9e309c40604c465ed012a73755504594"
    }
    @GET("onecall")
    @Headers("Content-Type: application/json")
    fun getWeatherAsync(
        @Query("lat") lat: Float,
        @Query("lon") lon: Float,
        @Query("units") units: String = "metric",
        @Query("exclude") exclude: List<String> = listOf("minutely"),
        @Query("appid") appid: String = API_KEY
    ): Deferred<Weather>

    @GET("weather")
    @Headers("Content-Type: application/json")
    fun getCoordinatesByCityNameAsync(
        @Query("q") cityName: String,
        @Query("appid") appid: String = API_KEY
    ): Deferred<City>
}