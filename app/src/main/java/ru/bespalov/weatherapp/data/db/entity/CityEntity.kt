package ru.bespalov.weatherapp.data.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import ru.bespalov.weatherapp.domain.model.City
import ru.bespalov.weatherapp.domain.model.Coordinate


@Entity(
    tableName = "cities"
)
data class CityEntity(
    @PrimaryKey val name: String,
    val lat: Float,
    val lon: Float
) {
    fun toCity(): City = City(
        name = name,
        Coordinate(lat, lon)
    )
}

fun City.toCityEntity(): CityEntity = CityEntity(
    name = name,
    lat = coordinate.lat,
    lon = coordinate.lon
)