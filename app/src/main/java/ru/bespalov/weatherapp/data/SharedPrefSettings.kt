package ru.bespalov.weatherapp.data

import android.content.Context
import android.util.Log
import dagger.hilt.android.qualifiers.ApplicationContext
import ru.bespalov.weatherapp.domain.AppSettings
import ru.bespalov.weatherapp.domain.model.Coordinate
import javax.inject.Inject

class SharedPrefSettings @Inject constructor(
    @ApplicationContext appContext: Context
) : AppSettings {

    companion object {
        const val CURRENT_CITY_KEY = "CURRENT_CITY_KEY"
        const val CURRENT_CITY_LAT_KEY = "CURRENT_CITY_LAT_KEY"
        const val CURRENT_CITY_LON_KEY = "CURRENT_CITY_LON_KEY"
        const val PREF_FILE_NAME = "weatherAppSettings"
    }

    private val sharedPreferences = appContext.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE)

    private val editor
        get() = sharedPreferences.edit()

    override fun getCurrentCity(): String? {
        return sharedPreferences.getString(CURRENT_CITY_KEY, "")
    }

    override fun setCurrentCity(cityName: String) {
        editor.putString(CURRENT_CITY_KEY, cityName).commit()
    }

    override fun setCurrentCityCoordinate(lat: Float, lon: Float) {
        editor.putFloat(CURRENT_CITY_LAT_KEY, lat).commit()
        editor.putFloat(CURRENT_CITY_LON_KEY, lon).commit()
    }

    override fun getCurrentCityCoordinate(): Coordinate {
        return Coordinate(
            sharedPreferences.getFloat(CURRENT_CITY_LAT_KEY, 0.0f),
            sharedPreferences.getFloat(CURRENT_CITY_LON_KEY, 0.0f)
        )
    }

    override fun isCurrentCityExists(): Boolean {
        return sharedPreferences.contains(CURRENT_CITY_KEY)
    }
}