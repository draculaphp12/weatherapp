package ru.bespalov.weatherapp.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ru.bespalov.weatherapp.data.db.entity.CityEntity

@Dao
interface CityDao {
    @Query("SELECT * FROM cities")
    suspend fun getAllCities(): List<CityEntity?>?

    @Insert
    suspend fun addCity(cityEntity: CityEntity)
}