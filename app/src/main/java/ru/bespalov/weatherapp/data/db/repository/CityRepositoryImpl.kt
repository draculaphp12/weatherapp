package ru.bespalov.weatherapp.data.db.repository

import ru.bespalov.weatherapp.data.db.dao.CityDao
import ru.bespalov.weatherapp.data.db.entity.toCityEntity
import ru.bespalov.weatherapp.domain.CityRepository
import ru.bespalov.weatherapp.domain.model.City
import javax.inject.Inject

class CityRepositoryImpl @Inject constructor(
    private val cityDao: CityDao
) : CityRepository {
    override suspend fun getAll(): List<City?>? {
        return cityDao.getAllCities()?.map { it?.toCity() }
    }

    override suspend fun addCity(city: City) {
        cityDao.addCity(city.toCityEntity())
    }
}