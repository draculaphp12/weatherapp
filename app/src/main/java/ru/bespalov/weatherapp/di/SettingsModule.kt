package ru.bespalov.weatherapp.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ru.bespalov.weatherapp.data.SharedPrefSettings
import ru.bespalov.weatherapp.domain.AppSettings

@Module
@InstallIn(SingletonComponent::class)
abstract class SettingsModule {

    @Binds
    abstract fun bindAppSettings(
        appSettings: SharedPrefSettings
    ): AppSettings
}