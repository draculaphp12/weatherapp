package ru.bespalov.weatherapp.di

import android.content.Context
import androidx.room.Room
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ru.bespalov.weatherapp.data.db.AppDatabase
import ru.bespalov.weatherapp.data.db.dao.CityDao
import ru.bespalov.weatherapp.data.db.repository.CityRepositoryImpl
import ru.bespalov.weatherapp.domain.CityRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class DatabaseModule {
    companion object {
        @Provides
        @Singleton
        fun provideDatabase(@ApplicationContext applicationContext: Context): AppDatabase {
            return Room.databaseBuilder(applicationContext, AppDatabase::class.java, "weatherDB")
                .build()
        }

        @Provides
        @Singleton
        fun provideCityDao(database: AppDatabase): CityDao {
            return database.getCityDao()
        }
    }

    @Binds
    @Singleton
    abstract fun bindCityRepository(
        cityRepository: CityRepositoryImpl
    ): CityRepository
}